﻿using System;

namespace Lab2
{
	class Newton
	{
		private readonly double root;
		private readonly double number;
		private readonly double accuracy;
		private double previous;
		private double current;

		public Newton(string rootString, string numberString, string accuracyString)
		{
			try
			{
				root = Convert.ToDouble(rootString.Replace(',', '.'));
				number = Convert.ToDouble(numberString.Replace(',', '.'));
				accuracy = Convert.ToDouble(accuracyString.Replace(',', '.'));
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public string Calculate()
		{
			try
			{
				previous = number / root;
				current = (1 / root) * ((root - 1) * previous + number / Math.Pow(previous, root - 1));
				while (Math.Abs(current - previous) > accuracy)
				{
					previous = current;
					current = (1 / root) * ((root - 1) * previous + number / Math.Pow(previous, root - 1));
				}
				return current.ToString();
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public string Check()
		{
			try
			{
				var pow = Math.Pow(current, root);
				return (number > pow) ? ">" : (number == pow) ? "=" : "<";
			}
			catch (Exception e)
			{
				throw e;
			}
		}
	}
}
