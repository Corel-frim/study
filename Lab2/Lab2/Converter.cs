﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab2
{
	class Converter
	{
		private readonly int number;

		public Converter(string number)
		{
			try
			{
				this.number = Convert.ToInt32(number);
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public string SystemConvert()
		{
			return Convert.ToString(number, 2);
		}

		public string Custom()
		{
			try
			{
				var count = number;
				var arr = new List<string>();
				while (count >= 1)
				{
					arr.Add(Convert.ToString(count % 2));
					count /= 2;
				}
				arr.Reverse();
				var sb = new StringBuilder();
				foreach (var t in arr)
				{
					sb.Append(t);
				}
				return sb.ToString();
			}
			catch (Exception e)
			{
				throw e;
			}
		}
	}
}
