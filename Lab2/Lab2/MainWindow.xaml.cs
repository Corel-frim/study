﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Input;

namespace Lab2
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			CultureInfo.CurrentCulture = new CultureInfo("en-US");
			InitializeComponent();
		}

		private void Converter(bool system)
		{
			try
			{
				if (ConvertableTextBox.Text == string.Empty)
				{
					MessageBox.Show("Enter the number");
					ConvertableTextBox.Focus();
					return;
				}
				var c = new Converter(ConvertableTextBox.Text);
				ConvertResultTextBox.Text = (system) ? c.SystemConvert() : c.Custom();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.ToString());
			}
		}

		private void Newton()
		{
			try
			{
				if (RootTextBox.Text == string.Empty || NumberTextBox.Text == string.Empty || AccuracyTextBox.Text == string.Empty)
				{
					MessageBox.Show("Enter all values");
					if (NumberTextBox.Text == string.Empty)
						NumberTextBox.Focus();
					else if (RootTextBox.Text == string.Empty)
						RootTextBox.Focus();
					else if (AccuracyTextBox.Text == string.Empty)
						AccuracyTextBox.Focus();
					return;
				}
				var n = new Newton(RootTextBox.Text, NumberTextBox.Text, AccuracyTextBox.Text);
				MathPowLabel.Visibility = Visibility.Visible;
				ResultTextBox.Text = n.Calculate();
				CheckLabel.Content = n.Check();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.ToString());
			}
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			Newton();
		}

		private void StandartConvertButton_Click(object sender, RoutedEventArgs e)
		{
			Converter(true);
		}

		private void CustomConvertButton_Click(object sender, RoutedEventArgs e)
		{
			Converter(false);
		}

		private void NewtonTextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (!(e.Key >= Key.D0 && e.Key <= Key.D9) && !(e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9) && e.Key != Key.Decimal)
			{
				if (e.Key == Key.Enter)
				{
					Newton();
				}
				e.Handled = true;
			}
		}

		private void ConvertableTextBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (!(e.Key >= Key.D0 && e.Key <= Key.D9) && !(e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
			{
				if (e.Key == Key.Enter)
				{
					Converter(true);
				}
				e.Handled = true;
			}
		}
	}
}
