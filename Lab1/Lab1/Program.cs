﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Lab1
{
	class Program
	{
		static List<(double x, double y)> list = new List<(double x, double y)>();

		public static void Main(string[] args)
		{
			try
			{
				CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("en-US");
				foreach (string arg in args)
				{
					Console.WriteLine($"Opened file in path: {arg}");
					FileInput(arg);
				}
				if (!args.Any())
				{
					PersonalInput();
				}

				foreach (var (x, y) in list)
				{
					Console.WriteLine($"X: {x} Y: {y}");
				}

				Console.ReadKey();
			}
			catch (Exception e)
			{
				Console.WriteLine("Something went wrong");
				Console.WriteLine(e);
			}
		}

		public static void FileInput(string path)
		{
			try
			{
				var fileText = File.ReadAllLines(path);
				foreach (var line in fileText)
				{
					var input = line.Split(',');
					if (input.Length != 2)
					{
						Console.WriteLine("Wrong parameters");
						continue;
					}
					list.Add((double.Parse(input[0]), double.Parse(input[1])));
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Something went wrong");
				Console.WriteLine(e);
			}
		}

		public static void PersonalInput()
		{
			try
			{
				Console.WriteLine("Enter empty string to finish reading");
				while (true)
				{
					Console.WriteLine("Enter X and  Y:");
					var s = Console.ReadLine();
					if (s == string.Empty)
					{
						break;
					}
					var input = s.Split(',');
					if (input.Length != 2)
					{
						Console.WriteLine("Wrong parameters");
						continue;
					}
					list.Add((double.Parse(input[0]), double.Parse(input[1])));
				}
			}
			catch(Exception e)
			{
				Console.WriteLine("Something went wrong");
				Console.WriteLine(e);
			}
		}
	}
}
